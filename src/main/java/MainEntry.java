import edu.uci.spiderlab.trustfulpool.entity.Requester;
import edu.uci.spiderlab.trustfulpool.entity.WorkerPool;

/*
@author Charles.Y.Feng
@date Oct 5, 2016 1:33:47 PM
*/

public class MainEntry {
	public static void main(String... args){
		ExperimentEngine engine = new ExperimentEngine(); 
		engine.init();
		engine.startSimulate();
	}
}
