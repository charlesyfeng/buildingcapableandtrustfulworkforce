import java.util.ArrayList;
import java.util.List;

import edu.uci.spiderlab.trustfolpool.mechanism.BehaviorChangeMechanism;
import edu.uci.spiderlab.trustfolpool.mechanism.Mechanism;
import edu.uci.spiderlab.trustfolpool.mechanism.ZeroTolerenceMechanism;
import edu.uci.spiderlab.trustfulpool.config.SimConfig;
import edu.uci.spiderlab.trustfulpool.entity.Requester;
import edu.uci.spiderlab.trustfulpool.entity.Worker;
import edu.uci.spiderlab.trustfulpool.entity.WorkerPool;
import edu.uci.spiderlab.trustfulpool.util.WorkerFactory;

/*
@author Charles
@date 2016-10��-14 ����10:52:26
*/

public class ExperimentEngine {
	
	int initPoolSize;
	
	Requester requester;
	WorkerPool workerPool;
	
	List<MechanismName> mechanisms;
	
	public enum MechanismName{
		NoTolerence, SampleAndPay, BehaviorChange
	}
	
	public ExperimentEngine(){
		this.initPoolSize = SimConfig.WorkerForceSize;
		mechanisms = new ArrayList<MechanismName>();
	}
	
	public void init(){
		mechanisms.clear();
		mechanisms.add(MechanismName.NoTolerence);
//		mechanisms.add(MechanismName.SampleAndPay);
		mechanisms.add(MechanismName.BehaviorChange);
	}
	
	private void initializeWorkerPool(int initPool){
		this.workerPool = new WorkerPool();	
		for(int i=1;i<=initPool;i++){
			Worker worker = WorkerFactory.getInstace().hireOneWorker();
			workerPool.addFreshWorker(worker);
		}
	}
	
	public void startSimulate(){
		for(MechanismName mechanismName: mechanisms){
			
			this.initializeWorkerPool(this.initPoolSize);
			
			Mechanism mechanism = null;
			if(mechanismName == MechanismName.SampleAndPay){
//				mechanism = new SampleAndPayMechanism();
			}else if(mechanismName == MechanismName.BehaviorChange){
				mechanism = new BehaviorChangeMechanism(mechanismName.toString());
			}else if(mechanismName == MechanismName.NoTolerence){
				mechanism = new ZeroTolerenceMechanism(mechanismName.toString());
			}else{
				System.out.println("Wrong mechanism name! Failed to initialize mechanism");
				System.exit(1);
			}
			
			requester = new Requester(mechanism, this.workerPool);
			requester.startWorking();
		}
	}
	
	
}
