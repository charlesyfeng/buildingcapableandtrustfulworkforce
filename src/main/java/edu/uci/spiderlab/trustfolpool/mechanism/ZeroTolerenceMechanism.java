/*
@author Charles
@date 2016-10��-20 ����1:44:20
*/

package edu.uci.spiderlab.trustfolpool.mechanism;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import java.util.Random;

import edu.uci.spiderlab.trustfulpool.config.SimConfig;
import edu.uci.spiderlab.trustfulpool.entity.FinishSignal;
import edu.uci.spiderlab.trustfulpool.entity.Record;
import edu.uci.spiderlab.trustfulpool.entity.Worker;
import edu.uci.spiderlab.trustfulpool.entity.WorkerPool;
import edu.uci.spiderlab.trustfulpool.util.WorkingRecorder;

public class ZeroTolerenceMechanism extends Mechanism {

	public ZeroTolerenceMechanism(String name){
		this.recorder = new WorkingRecorder();
		this.name = name;
	}
	
	@Override
	public double checking(int workingRound, WorkerPool workerPool) {
		double cost = 0.0;
		Record wr = new Record();
		wr.roundIndex = workingRound;
		wr.setOrigWorkerSize(workerPool.workerMap.size());
		super.groundTruthChecking(workerPool, wr);

		
		List<Entry<Integer, Worker>> workerList = new ArrayList<Entry<Integer, Worker>>();
		workerList.addAll(workerPool.workerMap.entrySet());
		
		int sampleCounter = 0;
		Random rand = new Random();
		
		HashMap<FinishSignal,Worker> sampleTarget = new HashMap<FinishSignal,Worker>();
		for(Entry<Integer, Worker> entry: workerList){
			FinishSignal signal = entry.getValue().processingTasks();
			if(signal.quality == SimConfig.SignalQuality.Low){
				entry.getValue().getSalary(SimConfig.PaymentValue.Unchecked_ClaimedLow.value);
				cost += SimConfig.PaymentValue.Unchecked_ClaimedLow.value;
			}else{
				sampleTarget.put(signal, entry.getValue());
			}
		} 
		
		int sampleSize = (int)(SimConfig.sampleRatio * sampleTarget.size());
		wr.checkedReportNum = sampleSize;
		
		List<Entry<FinishSignal, Worker>> targetWorkers = new ArrayList<Entry<FinishSignal, Worker>>();
		targetWorkers.addAll(sampleTarget.entrySet());
		
		int kickOutCounter = 0;
		while(sampleCounter<sampleSize){
			int index = rand.nextInt(targetWorkers.size());
			Entry<FinishSignal, Worker> entry = targetWorkers.get(index);
			if(entry.getKey().isFraud){
				workerPool.kickoutWorker(entry.getValue().getWorkerId());
				kickOutCounter++;
			}else{
				entry.getValue().getSalary(SimConfig.PaymentValue.Checked_ClaimedHigh_RealHigh.value);
				cost += SimConfig.PaymentValue.Checked_ClaimedHigh_RealHigh.value;
			}
			targetWorkers.remove(entry);
			sampleCounter++;
		}
		wr.kickoutWorkerNum = kickOutCounter;
		
		for(Entry<FinishSignal,Worker> entry: targetWorkers){
			//pay the left guys unchecked high payment
			entry.getValue().getSalary(SimConfig.PaymentValue.Unchecked_ClaimedHigh.value);
			cost += SimConfig.PaymentValue.Unchecked_ClaimedHigh.value;
		}
		
		wr.totalCost = cost;
		wr.leftWorkerNum = workerPool.workerMap.size();
		this.recorder.insertNewRecord(wr);
		super.printWorkingInfosOnConsole(wr);
		return cost;
	}
}
