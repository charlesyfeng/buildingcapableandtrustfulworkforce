///*
//@author Charles
//@date 2016-10月-20 下午3:45:33
//*/
//
//package edu.uci.spiderlab.trustfolpool.mechanism;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Random;
//import java.util.Map.Entry;
//
//import edu.uci.spiderlab.trustfulpool.config.SimConfig;
//import edu.uci.spiderlab.trustfulpool.entity.FinishSignal;
//import edu.uci.spiderlab.trustfulpool.entity.Record;
//import edu.uci.spiderlab.trustfulpool.entity.Worker;
//import edu.uci.spiderlab.trustfulpool.entity.WorkerPool;
//
//public class SampleAndPayMechanism extends Mechanism{
//
//	/**
//	 * 我对每个worker的报告按照设定的比例进行sample，然后对sample的每个report进行审核。
//	 * 审核完成之后。按照被检测到的fraud的比例和reporter claim的
//	 * report的quality进行支付。比如说我们对worker A提交的reports中，
//	 * sample出来10个，并且检测到有3个是fraud的。
//	 * 那么我们只支付用户claim的payment的70%。
//	 * 该操作中不进行worker的踢出。
//	 */
//	@Override
//	public double checking(int workingRound, WorkerPool workerPool) {
//		double cost = 0.0;
//		Record wr = new Record();
//		wr.roundIndex = workingRound;
//		wr.setOrigWorkerSize(workerPool.workerMap.size());
//		
//		List<Entry<Integer, Worker>> workerList = new ArrayList<Entry<Integer, Worker>>();
//		workerList.addAll(workerPool.workerMap.entrySet());
//		
//		int sampleCounter = 0;
//		Random rand = new Random();
//		
//		HashMap<FinishSignal,Worker> sampleTarget = new HashMap<FinishSignal,Worker>();
//		for(Entry<Integer, Worker> entry: workerList){
//			FinishSignal signal = entry.getValue().processingTasks();
//			if(signal.quality == SimConfig.SignalQuality.Low){
//				entry.getValue().getSalary(SimConfig.PaymentValue.Unchecked_ClaimedLow.value);
//				cost += SimConfig.PaymentValue.Unchecked_ClaimedLow.value;
//			}else{
//				sampleTarget.put(signal, entry.getValue());
//			}
//		} 
//		
//		int sampleSize = (int)SimConfig.sampleRatio * sampleTarget.size();
//		wr.checkedReportNum = sampleSize;
//		
//		List<Entry<FinishSignal, Worker>> targetWorkers = new ArrayList<Entry<FinishSignal, Worker>>();
//		targetWorkers.addAll(sampleTarget.entrySet());
//		
//		int kickOutCounter = 0;
//		while(sampleCounter<sampleSize){
//			int index = rand.nextInt(targetWorkers.size());
//			Entry<FinishSignal, Worker> entry = targetWorkers.get(index);
//			if(entry.getKey().isFraud){
////				We will not kick out anyone, just pay 
//				
//				
//			}else{
//				entry.getValue().getSalary(SimConfig.PaymentValue.Checked_ClaimedHigh_RealHigh.value);
//				cost += SimConfig.PaymentValue.Checked_ClaimedHigh_RealHigh.value;
//			}
//			targetWorkers.remove(entry);
//			sampleCounter++;
//		}
//		wr.kickoutWorkerNum = kickOutCounter;
//		
//		for(Entry<FinishSignal,Worker> entry: targetWorkers){
//			//pay the left guys unchecked high payment
//			entry.getValue().getSalary(SimConfig.PaymentValue.Unchecked_ClaimedHigh.value);
//			cost += SimConfig.PaymentValue.Unchecked_ClaimedHigh.value;
//		}
//		
//		wr.totalCost = cost;
//		wr.leftWorkerNum = workerPool.workerMap.size();
//		this.recorder.insertNewRecord(wr);
//		super.printWorkingInfosOnConsole(wr);
//		return cost;
//	}
//
//}
