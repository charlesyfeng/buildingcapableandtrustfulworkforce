/*
@author Charles
@date 2016-10��-20 ����1:57:05
*/

package edu.uci.spiderlab.trustfolpool.mechanism;

import java.util.Map.Entry;

import edu.uci.spiderlab.trustfulpool.config.SimConfig;
import edu.uci.spiderlab.trustfulpool.config.SimConfig.SignalQuality;
import edu.uci.spiderlab.trustfulpool.entity.FinishSignal;
import edu.uci.spiderlab.trustfulpool.entity.Record;
import edu.uci.spiderlab.trustfulpool.entity.Worker;
import edu.uci.spiderlab.trustfulpool.entity.WorkerPool;
import edu.uci.spiderlab.trustfulpool.util.WorkingRecorder;

public abstract class Mechanism {
	
	public WorkingRecorder recorder;
	public String name;
	
	//return the total monetary cost
	public abstract double checking(int roundIndex, WorkerPool workerPool);
	
	protected void printWorkingInfosOnConsole(Record r){
		System.out.println(r.formatPrint());
	}
	
	public void groundTruthChecking(WorkerPool workerPool, Record wr){
		int fraudCounter = 0;
		double utilityGain = 0.0;
		int LQReport = 0;
		int HQReport = 0;
		
		for(Entry<Integer,Worker> entry: workerPool.workerMap.entrySet()){
			FinishSignal signal = entry.getValue().processingTasks();
			if(signal.isFraud){
				fraudCounter++;
				utilityGain+= SimConfig.Low_Utility;
				LQReport++;
			}else{
				if(signal.quality == SignalQuality.High){
					HQReport++;
					utilityGain+= SimConfig.High_Utility;
				}else{
					utilityGain+= SimConfig.Low_Utility;
					LQReport++;
				}
			}
		}
		wr.fraudCounter = fraudCounter;
		wr.LQReport = LQReport;
		wr.HQReport = HQReport;
		wr.utilityGain = utilityGain;
	}
}
