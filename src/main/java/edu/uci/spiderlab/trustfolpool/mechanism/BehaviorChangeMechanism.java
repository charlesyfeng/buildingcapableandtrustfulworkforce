/*
@author Charles
@date 2016-10��-20 ����3:41:35
*/

package edu.uci.spiderlab.trustfolpool.mechanism;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import edu.uci.spiderlab.trustfulpool.config.SimConfig;
import edu.uci.spiderlab.trustfulpool.config.SimConfig.SignalQuality;
import edu.uci.spiderlab.trustfulpool.entity.FinishSignal;
import edu.uci.spiderlab.trustfulpool.entity.Record;
import edu.uci.spiderlab.trustfulpool.entity.Worker;
import edu.uci.spiderlab.trustfulpool.entity.WorkerPool;
import edu.uci.spiderlab.trustfulpool.util.WorkingRecorder;

public class BehaviorChangeMechanism extends Mechanism {

	public BehaviorChangeMechanism(String name){
		this.recorder = new WorkingRecorder();
		this.name = name;
	}
	
	@Override
	public double checking(int workingRound,WorkerPool workerPool) {
		double cost = 0.0;
		Record wr = new Record();
		wr.roundIndex = workingRound;
		wr.setOrigWorkerSize(workerPool.workerMap.size());
		super.groundTruthChecking(workerPool, wr);

		int inspectingWorkerSize = (int) (workerPool.workerMap.size() * SimConfig.sampleRatio);

		Random rand = new Random();

		List<Entry<Integer, Worker>> workerList = new ArrayList<Entry<Integer, Worker>>();
		workerList.addAll(workerPool.workerMap.entrySet());

		List<Entry<Integer, Worker>> warnedWorkerList = new ArrayList<Entry<Integer, Worker>>();
		for (Entry<Integer, Worker> entry : workerList) {
			if (entry.getValue().status == Worker.WorkerStatus.Warned) {
				warnedWorkerList.add(entry);
			}
		}
		workerList.removeAll(warnedWorkerList);

		int inspectedCounter = 0;
		int kickOutCounter =0;
		for (Entry<Integer, Worker> entry : warnedWorkerList) {
			FinishSignal signal = entry.getValue().processingTasks();
			if(signal.quality == SignalQuality.High){
				if(signal.isFraud){
					entry.getValue().getSalary(SimConfig.PaymentValue.Cheat.value);
					workerPool.kickoutWorker(entry.getValue().getWorkerId());
					kickOutCounter++;
				}else{
					entry.getValue().getSalary(SimConfig.PaymentValue.Checked_ClaimedHigh_RealHigh.value);
					cost+= SimConfig.PaymentValue.Checked_ClaimedHigh_RealHigh.value;
					entry.getValue().status = Worker.WorkerStatus.Normal; //flag out the warned worker
					entry.getValue().decreChangeBehavior();
				}
				inspectedCounter++;
			}else{
				entry.getValue().getSalary(SimConfig.PaymentValue.Unchecked_ClaimedLow.value);
			}
		}
		wr.kickoutWorkerNum = kickOutCounter;
		
		
		if(inspectedCounter > inspectingWorkerSize){
			//intentionally do nothing;
		}else{
			int workingSize = inspectingWorkerSize - inspectedCounter;
			
			HashMap<FinishSignal, Worker> signalToWorker = new HashMap<FinishSignal, Worker>();
			for(Entry<Integer, Worker> workerEntry : workerList){
				signalToWorker.put(workerEntry.getValue().processingTasks(), workerEntry.getValue());
			}

			List<Entry<FinishSignal, Worker>> goodWorkerList = new ArrayList<Entry<FinishSignal, Worker>>();
			goodWorkerList.addAll(signalToWorker.entrySet());
			
			List<Entry<FinishSignal, Worker>> highWorkerList = new ArrayList<Entry<FinishSignal, Worker>>();
			
			for(Entry<FinishSignal, Worker> entry : goodWorkerList){
				if(entry.getKey().quality == SignalQuality.High){
					highWorkerList.add(entry);
				}else{
					entry.getValue().getSalary(SimConfig.PaymentValue.Unchecked_ClaimedLow.value);
					cost += SimConfig.PaymentValue.Unchecked_ClaimedLow.value;
				}
			}
			
			if(highWorkerList.size() < workingSize){
				workingSize = highWorkerList.size(); 
			}
			int i =0;
			int warnedCounter = 0;
			while(i<workingSize){
				int index = rand.nextInt(highWorkerList.size());
				Entry<FinishSignal, Worker> entry = highWorkerList.get(index);
				if(entry.getKey().isFraud){
					entry.getValue().status = Worker.WorkerStatus.Warned;
					entry.getValue().getSalary(SimConfig.PaymentValue.Cheat.value);
					entry.getValue().increChangeBehavior();
					warnedCounter++;
				}else{
					entry.getValue().getSalary(SimConfig.PaymentValue.Checked_ClaimedHigh_RealHigh.value);
					cost += SimConfig.PaymentValue.Checked_ClaimedHigh_RealHigh.value;
				}
				highWorkerList.remove(index);
				i++;
				inspectedCounter++;
			}
			
			for(Entry<FinishSignal, Worker> entry : highWorkerList){
				entry.getValue().getSalary(SimConfig.PaymentValue.Unchecked_ClaimedHigh.value);
				cost += SimConfig.PaymentValue.Unchecked_ClaimedHigh.value;
			}
			
			wr.setWarnedWorkerNum(warnedCounter);
			wr.setCheckedReportNum(inspectedCounter);
		}
		
		wr.setTotalCost(cost);
		wr.setLeftWorkerNum(workerPool.workerMap.size());
		
		super.recorder.insertNewRecord(wr);
		super.printWorkingInfosOnConsole(wr);
		return cost;
	}
}
