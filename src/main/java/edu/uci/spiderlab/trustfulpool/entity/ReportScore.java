/*
@author Charles.Y.Feng
@date Oct 5, 2016 3:16:59 PM
*/

package edu.uci.spiderlab.trustfulpool.entity;

public enum ReportScore {
//	Excellent, Good, Failed
	High, Low
}
