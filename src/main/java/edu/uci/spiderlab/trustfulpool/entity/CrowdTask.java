/*
@author Charles.Y.Feng
@date Oct 5, 2016 1:32:42 PM
*/

package edu.uci.spiderlab.trustfulpool.entity;

import edu.uci.spiderlab.trustfulpool.config.SimConfig;
import edu.uci.spiderlab.trustfulpool.util.ScoreValueGenerator;

public class CrowdTask {
	
	private static ScoreValueGenerator diffEst = 
			new ScoreValueGenerator(SimConfig.skillScoreMin,SimConfig.skillScoreMax); // estimate the difficulty of crowdTasks
	
	public double difficultyValue;
	
	public CrowdTask(){
		this.difficultyValue = diffEst.randomGetNormalDistributionScore();
	}
}
