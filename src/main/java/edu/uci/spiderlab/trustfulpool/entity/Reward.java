/*
@author Charles.Y.Feng
@date Oct 5, 2016 3:22:35 PM
*/

package edu.uci.spiderlab.trustfulpool.entity;

public class Reward {
	public double value;
	
	public Reward(double value){
		this.value = value;
	}
}
