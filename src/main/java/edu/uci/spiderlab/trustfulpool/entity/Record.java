/*
@author Charles
@date 2016-10��-23 ����1:41:33
*/

package edu.uci.spiderlab.trustfulpool.entity;

public class Record{
	public int roundIndex;
	public int origWorkerSize;
	public int kickoutWorkerNum;
	public int warnedWorkerNum;
	public int leftWorkerNum;
	public int checkedReportNum;
	public double totalCost;
	
	public int fraudCounter;
	public double utilityGain = 0.0;
	public int LQReport = 0;
	public int HQReport = 0;
	
	
	public Record(){
		
	}

	public void setRoundIndex(int roundIndex) {
		this.roundIndex = roundIndex;
	}

	public void setOrigWorkerSize(int origWorkerSize) {
		this.origWorkerSize = origWorkerSize;
	}

	public void setKickoutWorkerNum(int kickoutWorkerNum) {
		this.kickoutWorkerNum = kickoutWorkerNum;
	}

	public void setWarnedWorkerNum(int warnedWorkerNum) {
		this.warnedWorkerNum = warnedWorkerNum;
	}

	public void setLeftWorkerNum(int leftWorkerNum) {
		this.leftWorkerNum = leftWorkerNum;
	}

	public void setCheckedReportNum(int checkedReportNum) {
		this.checkedReportNum = checkedReportNum;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public int getRoundIndex() {
		return roundIndex;
	}

	public int getOrigWorkerSize() {
		return origWorkerSize;
	}

	public int getKickoutWorkerNum() {
		return kickoutWorkerNum;
	}

	public int getWarnedWorkerNum() {
		return warnedWorkerNum;
	}

	public int getLeftWorkerNum() {
		return leftWorkerNum;
	}

	public int getCheckedReportNum() {
		return checkedReportNum;
	}

	public double getTotalCost() {
		return totalCost;
	}
	
	public String formatPrint(){
		return String.format("Round = %d, origWorkerNumber = %d, kickoutWorkerNumber = %d, "
				+ "warnedWorkerNumber = %d, remainingWorkerNumber = %d, checkedReportNumber = %d, totalCost = %f \n "
				+ "totalUtility = %f, gtFraudCounter = %d, HQ = %d, LQ = %d", this.roundIndex,
				this.origWorkerSize, this.kickoutWorkerNum, this.warnedWorkerNum, this.leftWorkerNum, this.checkedReportNum, this.totalCost,
				this.utilityGain, this.fraudCounter, this.HQReport, this.LQReport
				);
	}
}