/*
@author Charles
@date 2016-10��-22 ����11:06:14
*/

package edu.uci.spiderlab.trustfulpool.entity;

import java.util.ArrayList;
import java.util.List;

import edu.uci.spiderlab.trustfulpool.config.SimConfig.ReportQuality;
import edu.uci.spiderlab.trustfulpool.config.SimConfig.SignalQuality;

public class FinishSignal {
	public SignalQuality quality;
	public boolean isFraud;

	public List<ReportQuality> reportQuality;
	
	public FinishSignal(SignalQuality quality, boolean isFraud){
		this.quality = quality;
		this.isFraud = isFraud;
		reportQuality = new ArrayList<ReportQuality>();
	}
}
