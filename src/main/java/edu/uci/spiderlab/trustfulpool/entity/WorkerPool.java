/*
@author Charles.Y.Feng
@date Oct 5, 2016 1:32:31 PM
*/

package edu.uci.spiderlab.trustfulpool.entity;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import edu.uci.spiderlab.trustfulpool.entity.Worker.WorkerStatus;

public class WorkerPool {
	
	public LinkedHashMap<Integer, Worker> workerMap;
	public Set<Worker> kickOutSet;
	
	public WorkerPool(){
		workerMap = new LinkedHashMap<Integer, Worker>();
		kickOutSet = new HashSet<Worker>();
	}
	
	public void addFreshWorker(Worker worker){
		this.workerMap.put(worker.getWorkerId(), worker);
	}
	
	public Worker findWorkerById(int id){
		return workerMap.get(id);
	}
	
	public void kickoutWorker(int id){
		Worker worker = this.findWorkerById(id);
		worker.status = WorkerStatus.KickedOut;
		this.workerMap.remove(id);
		this.kickOutSet.add(worker);
	}
	
	public void printAllWorkerInfosOnConsole(){
		System.out.println(String.format("Worker Pool Infomation : size = %d", this.workerMap.size()));
		for(Integer key : workerMap.keySet()){
			System.out.println(String.format("WorkerId = %d, Status = %s, Infos = %s", key, 
					workerMap.get(key).status, workerMap.get(key).printWorkerInfos()));
		}
	}
	
	public WorkerPool copy(){
		WorkerPool copyObj = new WorkerPool();
		copyObj.workerMap.putAll(this.workerMap);
		copyObj.kickOutSet.addAll(this.kickOutSet);
		return copyObj;
	} 
}
