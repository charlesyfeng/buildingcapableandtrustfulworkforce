/*
@author Charles.Y.Feng
@date Oct 5, 2016 1:30:08 PM
*/

package edu.uci.spiderlab.trustfulpool.entity;

import java.util.ArrayList;
import java.util.List;

import edu.uci.spiderlab.trustfulpool.config.SimConfig;
import edu.uci.spiderlab.trustfulpool.config.SimConfig.ReportQuality;
import edu.uci.spiderlab.trustfulpool.config.SimConfig.SignalQuality;
import edu.uci.spiderlab.trustfulpool.util.ScoreValueGenerator;

public class Worker {

	public enum WorkerStatus{
		Normal, Warned, KickedOut
	}
	
	private static ScoreValueGenerator fraudWilling = 
			new ScoreValueGenerator(SimConfig.integrityMin,SimConfig.integrityMax); // estimate the difficulty of crowdTasks
	
	private int workerId;
	
	public WorkerStatus status;
	
	private final double integrityValue; // the cheating probability, from 0~1, should follow some distribution
	
	private final double skillScore; // from 0~1, should following Normal Distribution
	
	private double changeBehaviorCounter;
	
	private List<CrowdTask> currentTaskPackage;
	
	private FinishSignal signal;
	
	private List<Double> paymentRecord;
	
	public Worker(int workerId, double integrityValue, double skillScore){
		this.workerId = workerId;
		this.integrityValue = integrityValue;
		this.skillScore = skillScore;
		this.paymentRecord = new ArrayList<Double>();
		this.changeBehaviorCounter = 0;
	}
	
	public void acceptTaskPackage(List<CrowdTask> taskPackage){
		this.currentTaskPackage = taskPackage;
		this.signal = null;
	}
	
	public FinishSignal processingTasks(){
		if(this.signal != null)
			return signal;
		
		double successfulFinished = 0;
		
		List<ReportQuality> reportDetails = new ArrayList<ReportQuality>();  
		
		for(CrowdTask crowdTask : this.currentTaskPackage){
			int res = workingOnTask(crowdTask);
			if(res == 1)
				reportDetails.add(ReportQuality.High);
			else 
				reportDetails.add(ReportQuality.Low);
			successfulFinished+=res;
		}
		
		int packageSize = this.currentTaskPackage.size();
		boolean isFraud = false;
		SignalQuality quality = SimConfig.SignalQuality.Low;
		
		if(successfulFinished/packageSize > SimConfig.cheatIntentionThreshold){
			quality = SimConfig.SignalQuality.High;
			isFraud = false;
		}else{
			double fraudProb = fraudWilling.randomGetNormalDistributionScore();
			isFraud = fraudProb > this.getExecutiveCheatProf();
			if(isFraud)
				quality = SimConfig.SignalQuality.High;
		}
		
		this.signal = new FinishSignal(quality, isFraud);
		signal.reportQuality = reportDetails;
		this.currentTaskPackage = null;
		return signal;
	}
	
	//return 1 means successfully finished
	//return 0 means failed
	private int workingOnTask(CrowdTask crowdTask){
		if(skillScore >= crowdTask.difficultyValue){
			return 1;
		}else{
			return 0;
		}
	}

	public double getExecutiveCheatProf(){
		return this.integrityValue * Math.pow(SimConfig.behaviorChangeFactor, this.changeBehaviorCounter);
	}
	
	public int getWorkerId() {
		return workerId;
	}

	public void setWorkerId(int workerId) {
		this.workerId = workerId;
	}


	public double getSkillScore() {
		return skillScore;
	}

	public String printWorkerInfos(){
		return String.format("IntegrityValue = %f, SkillScore = %f", this.integrityValue, this.skillScore);
	}
	
	public void increChangeBehavior(){
		if(this.changeBehaviorCounter < SimConfig.maxWarningTimes)
			this.changeBehaviorCounter++;
		else
			System.out.println("Could not increase behavior counter! The maxTimes = "+ SimConfig.maxWarningTimes);
	}
	
	public void decreChangeBehavior(){
		if(this.changeBehaviorCounter > 0)
			this.changeBehaviorCounter--;
		else
			System.out.println("Could not decrease behavior counter!");
	}
	
	public void getSalary(Double payment){
		this.paymentRecord.add(payment);
	}
}
