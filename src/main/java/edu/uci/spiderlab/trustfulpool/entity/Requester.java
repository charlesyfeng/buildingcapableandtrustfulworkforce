/*
@author Charles.Y.Feng
@date Oct 5, 2016 1:30:15 PM
*/

package edu.uci.spiderlab.trustfulpool.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import edu.uci.spiderlab.trustfolpool.mechanism.Mechanism;
import edu.uci.spiderlab.trustfulpool.config.SimConfig;

public class Requester {

	private Mechanism mechanism;
	private WorkerPool workerForce;
	
	private int workingRound;
	
	private static class TaskFactory{

		public static CrowdTask getNextTask(){
			return new CrowdTask();
		}
		
		public static List<CrowdTask> generateTaskPackage(int packageSize){
			List<CrowdTask> res = new ArrayList<CrowdTask>();
			for(int i=0;i<packageSize; i++)
				res.add(TaskFactory.getNextTask());
			return res;
		}
		
	}
	
	public Requester(Mechanism mechanism, WorkerPool workerPool){
		this.mechanism = mechanism;
		this.workerForce = workerPool;
		this.workingRound = 0;
	}
	
	public void startWorking(){
		System.out.println("Name = "+this.mechanism.name+"; Start=============================>>>>>>>>>");
		for(this.workingRound=0; this.workingRound < SimConfig.workingRound; this.workingRound++){
			assignTasks();
			mechanism.checking(this.workingRound,workerForce);
		}
		System.out.println("Name = "+this.mechanism.name+"; End=============================>>>>>>>>>");
	}
	
	private void assignTasks(){
		for(Entry<Integer, Worker> entry: this.workerForce.workerMap.entrySet()){
			List<CrowdTask> taskPackage = Requester.TaskFactory.generateTaskPackage(SimConfig.taskPackageSize);
			entry.getValue().acceptTaskPackage(taskPackage);
		}
	}
	
	public void printSettingInfos(){
		String infos = String.format("Original WorkerSize = %d", this.workerForce.workerMap.size());
	}
}
