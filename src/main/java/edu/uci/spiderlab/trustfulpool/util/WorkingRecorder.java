/*
@author Charles
@date 2016-10��-23 ����12:40:23
*/

package edu.uci.spiderlab.trustfulpool.util;

import java.util.ArrayList;
import java.util.List;

import edu.uci.spiderlab.trustfulpool.entity.Record;

public class WorkingRecorder {
	List<Record> records;
	
	public WorkingRecorder(){
		this.records = new ArrayList<Record>();
	}
	
	public void insertNewRecord(Record wr){
		this.records.add(wr);
	}
	
}
