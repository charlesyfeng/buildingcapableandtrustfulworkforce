/*
@author Charles.Y.Feng
@date Oct 5, 2016 1:32:21 PM
*/

package edu.uci.spiderlab.trustfulpool.util;

import edu.uci.spiderlab.trustfulpool.config.SimConfig;
import edu.uci.spiderlab.trustfulpool.entity.Worker;

public class WorkerFactory {
	
	private static int workerIdCounter = 1;
	
	private static ScoreValueGenerator integrityScoreGenerator;
	
	private static ScoreValueGenerator skillScoreGenerator; 
	
	private static WorkerFactory ins; 
	
	public static WorkerFactory getInstace(){
		if(ins == null){
			ins = new WorkerFactory();
		}
		return ins;
	}
	
	private WorkerFactory(){
		integrityScoreGenerator = new ScoreValueGenerator(SimConfig.integrityMin, SimConfig.integrityMax);
		skillScoreGenerator = new ScoreValueGenerator(SimConfig.skillScoreMin, SimConfig.skillScoreMax);
	}
	
	public Worker hireOneWorker(){
		double integrity = integrityScoreGenerator.randomGetNormalDistributionScore();
		double skillScore = skillScoreGenerator.randomGetNormalDistributionScore();
		Worker worker = new Worker(getNextWorkerId(), integrity, skillScore);
		return worker;
	}
	
	private int getNextWorkerId(){
		return WorkerFactory.workerIdCounter++;
	}
}
