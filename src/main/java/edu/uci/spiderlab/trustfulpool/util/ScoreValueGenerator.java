/*
@author Charles.Y.Feng
@date Oct 5, 2016 1:29:50 PM
*/

package edu.uci.spiderlab.trustfulpool.util;


import org.apache.commons.math3.distribution.NormalDistribution;

public class ScoreValueGenerator {
	
	private NormalDistribution fRandom;
	double min;
	double max;
	
	/*
	 * We should set the mean value and the variance of the distribution.
	 * */
	public ScoreValueGenerator(double min, double max){
		this.min = min;
		this.max = max;
				
		fRandom = new NormalDistribution();
	}
	
	/*
	 * Returns the next pseudorandom, Gaussian ("normally") distributed
     * {@code double} value with mean {@code 0.0} and standard
     * deviation {@code 1.0} from this random number generator's sequence.
     * 
     * nextGaussian() Returns the next pseudorandom, Gaussian ("normally") 
     * distributed double value with mean 0.0 and 
     * standard deviation 1.0 from this random number generator's sequence. 
	 * */
	public double randomGetNormalDistributionScore(){
//		return this.fRandom.probability(this.min, this.max);
//		return this.fRandom.sample();
		
		while(true){
			double value = this.fRandom.sample();
			if(value>=-1 && value<=1)
				return value*0.5+0.5;
		}
	}
	
	public static void main(String... args){
		ScoreValueGenerator g = new ScoreValueGenerator(0,1);
		for(int i=0;i<10;i++)
		System.out.println(g.randomGetNormalDistributionScore());
	}
}
