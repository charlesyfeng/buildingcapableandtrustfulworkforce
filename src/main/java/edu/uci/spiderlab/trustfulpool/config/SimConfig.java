/*
@author Charles
@date 2016-10��-15 ����2:02:31
*/

package edu.uci.spiderlab.trustfulpool.config;

public class SimConfig {
	
	//for the normal distribution MEAN_VALUE and STD
	public final static double integrityMin = 0;
	public final static double integrityMax = 1;
	
	public final static double skillScoreMin = 0;
	public final static double skillScoreMax = 1;

	//worker force size
	public final static int WorkerForceSize = 1000;
	
	//payment values
	public final static double Checked_ClaimedHigh_RealHigh = 1.0;
	public final static double Unchecked_ClaimedHigh = 0.7;
	public final static double Unchecked_ClaimedLow = 0.2;
	public final static double Cheat = 0.0;
	
	//Utility
	public final static double High_Utility = 1;
	public final static double Low_Utility = 0.1;
	
	//change behavior params
	public final static double behaviorChangeFactor = 0.8;
	public final static int maxWarningTimes = 1;
	
	//system behavior param
	public final static double sampleRatio = 0.1;
	public final static int workingRound = 10;
	public final static int taskPackageSize = 10;
	public final static double cheatIntentionThreshold = 0.6;
	
	
	public enum PaymentValue{
		
		Checked_ClaimedHigh_RealHigh(SimConfig.Checked_ClaimedHigh_RealHigh),
		Unchecked_ClaimedHigh(SimConfig.Unchecked_ClaimedHigh),
		Unchecked_ClaimedLow(SimConfig.Unchecked_ClaimedLow),
		Cheat(SimConfig.Cheat);

		public double value;
		
		private PaymentValue(double payment){
			this.value = payment;
		}
	}
	
	public enum SignalQuality{
		High, Low;
	}
	
	public enum ReportQuality{
		High, Low;
	}
}
